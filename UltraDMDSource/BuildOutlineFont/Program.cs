﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace BuildOutlineFont
{
    class Program
    {
        public static Bitmap GenerateOutlineFont(Bitmap src, bool outline)
        {
            Int32 charWidthBefore = src.Width / 59;
            Int32 charWidthAfter = charWidthBefore+2;
            Int32 newWidth = charWidthAfter * 59;
            Int32 newHeight = src.Height + 2;

            Bitmap dst = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

            Color colWhite = Color.FromArgb(0xff, 0xff, 0xff);

            //paint white
            for (int y = 0; y < newHeight; y++)
            {
                for (int x = 0; x < newWidth; x++)
                {
                    dst.SetPixel(x, y, colWhite);
                }
            }

            if (outline)
            {
                //copy characters as outline
                for (int y = 0; y < src.Height; y++)
                {
                    for (int x = 0; x < src.Width; x++)
                    {
                        int iChar = x / charWidthBefore;
                        int x2 = iChar * charWidthAfter + (x % charWidthBefore) + 1;
                        int y2 = y+1;

                        Color col = src.GetPixel(x, y);
                        if (col.R == 0x00 && col.G == 0x00 && col.B == 0x00)
                        {
                            dst.SetPixel(x2 - 1, y2 - 1, col);
                            dst.SetPixel(x2 - 1, y2 + 0, col);
                            dst.SetPixel(x2 - 1, y2 + 1, col);

                            dst.SetPixel(x2 + 0, y2 - 1, col);
                            dst.SetPixel(x2 + 0, y2 + 0, col);
                            dst.SetPixel(x2 + 0, y2 + 1, col);

                            dst.SetPixel(x2 + 1, y2 - 1, col);
                            dst.SetPixel(x2 + 1, y2 + 0, col);
                            dst.SetPixel(x2 + 1, y2 + 1, col);
                        }
                    }
                }
            }

            Color colorFill = (outline ? colWhite : Color.FromArgb(0x00, 0x00, 0x00));
            //copy original char as white
            for (int y = 0; y < src.Height; y++)
            {
                for (int x = 0; x < src.Width; x++)
                {
                    int iChar = x / charWidthBefore;
                    int x2 = iChar * charWidthAfter + (x % charWidthBefore) + 1;
                    int y2 = y + 1;

                    Color col = src.GetPixel(x, y);
                    if (col.R == 0x00 && col.G == 0x00 && col.B == 0x00)
                    {
                        dst.SetPixel(x2 + 0, y2 + 0, colorFill);
                    }
                }
            }

            return dst;
        }

        static void Main(string[] args)
        {
            System.Diagnostics.Trace.WriteLine(args[0]);
            {
                Bitmap dst = GenerateOutlineFont(new Bitmap(args[0]), true);
                string dstPath = System.IO.Path.GetDirectoryName(args[0]);
                if (dstPath.Length == 0)
                {
                    dstPath = System.IO.Directory.GetCurrentDirectory();
                }
                string dstFilename = System.IO.Path.GetFileNameWithoutExtension(args[0]);
                string dstExt = System.IO.Path.GetExtension(args[0]);
                string newFilename = string.Format("{0}\\{1}.outline{2}", dstPath, dstFilename, dstExt);
                dst.Save(newFilename);
            }
            {
                Bitmap dst = GenerateOutlineFont(new Bitmap(args[0]), false);
                string dstPath = System.IO.Path.GetDirectoryName(args[0]);
                if (dstPath.Length == 0)
                {
                    dstPath = System.IO.Directory.GetCurrentDirectory();
                }
                string dstFilename = System.IO.Path.GetFileNameWithoutExtension(args[0]);
                string dstExt = System.IO.Path.GetExtension(args[0]);
                string newFilename = string.Format("{0}\\{1}.fill{2}", dstPath, dstFilename, dstExt);
                dst.Save(newFilename);
            }
        }
    }
}
