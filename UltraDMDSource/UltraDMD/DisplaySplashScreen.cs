﻿#region Using directives
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
#endregion

namespace UltraDMD
{
    public class DisplaySplashScreen : DisplayScene
    {
        private Int32 _majorVersion;
        private Int32 _minorVersion;
        private Int32 _buildNumber;
        private Int32 _step;

        public DisplaySplashScreen(Int32 majorVersion, Int32 minorVersion, Int32 buildNumber)
        {
            _majorVersion = majorVersion;
            _minorVersion = minorVersion;
            _buildNumber = buildNumber;
            _step = 0;
        }
        public override bool Show()
        {
            if (_step > 0)
            {
                return true;
            }
            ++_step;

            try
            {
                //string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

                Bitmap bmpUltraDMDXSm = Program.GetImageByName("UltraDMDXSm");
                Bitmap bmpPeepiXSm = Program.GetImageByName("PeepiXSm");

                XDMD.Surface SurfTitle = new XDMD.Surface(bmpUltraDMDXSm, Program.draw);
                XDMD.Surface SurfPeepi = new XDMD.Surface(bmpPeepiXSm, Program.draw);
                //XDMD.Font f = new XDMD.Font(exePath + "\\Fonts\\f4by5.gif", Program.draw);
                XDMD.Font f = new XDMD.Font(Program.GetImageByName("f4by5"), Program.draw);

                string ver = string.Format("v{0}.{1}.{2}", _majorVersion, _minorVersion, _buildNumber);
                Program.draw.TransitionSurface(SurfTitle, new Rectangle(0, -5, 128, 32), XDMD.Device.AnimationType.ScrollOnRight, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_08, 220, XDMD.Device.AnimationType.ScrollOffLeft, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_08);
                Program.draw.TransitionSurface(SurfPeepi, new Rectangle(8, 0, 21, 32), XDMD.Device.AnimationType.ScrollOnUp, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02, 190, XDMD.Device.AnimationType.ScrollOffDown, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02);
                Program.draw.TransitionFont(f, ver, 54, 20, XDMD.Device.AnimationType.ScrollOnUp, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02, 220, XDMD.Device.AnimationType.ScrollOffDown, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_02);
                Program.draw.TransitionFont(f, "powered by XDMD", 44, 26, XDMD.Device.AnimationType.ScrollOnLeft, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_06, 180, XDMD.Device.AnimationType.ScrollOffRight, XDMD.Device.AnimationType.None, XDMD.Device.AnimationSpeed.Speed_06);
            }
            catch (Exception ex)
            {
                throw ex; // Re-throw the exception
            }
            return false;
        }
    }
}
