﻿
#region Using directives
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
#endregion

namespace UltraDMD
{
    public static class MySubclassWindow
    {
        private delegate bool EnumWindowsProc(IntPtr testWindowHandle, int lParam);

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

        [DllImport("user32.dll")]
        private static extern bool EnumDesktopWindows(IntPtr hDesktop, EnumWindowsProc lpEnumFunc, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern IntPtr GetDesktopWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr SendMessageTimeout(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam, uint fuFlags, uint uTimeout, out IntPtr lpdwResult);

        private static List<string> windowTitles = new List<string>();

        public static void SubclassWindow()
        {
            EnumDesktopWindows(IntPtr.Zero, MySubclassWindow.EnumWindowsCallback, IntPtr.Zero);
        }

        public static List<string> GetWindowTitles(bool includeChildren)
        {
            //IntPtr hwndDesktop = GetDesktopWindow();
            EnumDesktopWindows(IntPtr.Zero, MySubclassWindow.EnumWindowsCallback, IntPtr.Zero);//includeChildren ? (IntPtr)1 : IntPtr.Zero);
            return MySubclassWindow.windowTitles;
        }

        private static bool EnumWindowsCallback(IntPtr testWindowHandle, int lParam)
        {
            string title = MySubclassWindow.GetWindowTitle(testWindowHandle);
            if (MySubclassWindow.TitleMatches(title))
            {
                SubclassHWND s = new SubclassHWND();
                s.AssignHandle(testWindowHandle);
                
                const short SWP_NOMOVE = 0X2;
                const short SWP_NOSIZE = 1;
                SetWindowPos(testWindowHandle, -1, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);


                return false;//no need to continue
            }
            return true;
        }

        private static string GetWindowTitle(IntPtr windowHandle)
        {
            uint SMTO_ABORTIFHUNG = 0x0002;
            uint WM_GETTEXT = 0xD;
            int MAX_STRING_SIZE = 32768;
            IntPtr result;
            string title = string.Empty;
            IntPtr memoryHandle = Marshal.AllocCoTaskMem(MAX_STRING_SIZE);
            Marshal.Copy(title.ToCharArray(), 0, memoryHandle, title.Length);
            MySubclassWindow.SendMessageTimeout(windowHandle, WM_GETTEXT, (IntPtr)MAX_STRING_SIZE, memoryHandle, SMTO_ABORTIFHUNG, (uint)1000, out result);
            title = Marshal.PtrToStringAuto(memoryHandle);
            Marshal.FreeCoTaskMem(memoryHandle);
            return title;
        }

        private static bool TitleMatches(string title)
        {
            if (title == null)
            {
                return false;
            }

            bool match = title.Contains("Virtual DMD");
            System.Diagnostics.Trace.WriteLine(title);
            return match;
        }

    }
}
